# Push Button Switch

[![CI Status Badge](https://ci.codeberg.org/api/badges/12377/status.svg)](https://ci.codeberg.org/7BIndustries/push-button-switch)

A 3D printed push button switch which can be used with a USB adapter and screen scanning software on computers and tablets.

![Full Switch Assembly with Base Attached](https://codeberg.org/7BIndustries/push-button-switch/raw/branch/main/docs/images/assembly_switch_with_straight_base_attached_on_a_desktop.jpg)

## Links

* [Documentation](https://7bindustries.com/hardware/push-button-switch/v1/index.html)
* [Download](http://7bindustries.com/static/downloads/push_button_switch/v1/Push_Button_Switch_v1_Download.zip)

## Contributing

If there are any errors, or if there are improvements to be made, please open an issue on this repository. Please be sure to follow the conduct guidelines within the [Python Community Code of Conduct](https://www.python.org/psf/conduct/) when doing so.

## Licenses

* [Apache Software License 2.0](LICENSES/apache.org_licenses_LICENSE-2.0.txt) - Software
* [CERN-OHL-P-2.0](LICENSES/CERN-OHL-P-2.0.txt) - Hardware
* [CC-BY-SA-4.0](LICENSES/CC-BY-SA-4.0.txt) - Documentation
